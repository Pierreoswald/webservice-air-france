package com.poswald.core.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
* User entity
* 
* @author Poswald
* 
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "USER")
public class User {

	/**
	 *  User entity id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 *  User login
	 */
	@Column(name = "login", nullable = false, length = 50)
	private String login;
	
	/**
	 *  User password
	 */
	@Column(name = "password", nullable = false, length = 50)
	private String password;
	
	/**
	 *  User entity first Name
	 */
	@Column(name = "first_name", length = 50)
	private String firstName;

	/**
	 *  User entity last Name
	 */
	@Column(name = "last_name", length = 50)
	private String lastName;

	/**
	 *  User email
	 */
	@Column(name = "email", nullable = false, length = 70)
	private String email;
	
	/**
	 *  User entity birthDate
	 */
	@Column(name = "birthdate", nullable = false)
	private Instant birthDate;

	/**
	 *  User entity country
	 */
	@Column(name = "country", nullable = false, length = 50)
	private String country;

}
