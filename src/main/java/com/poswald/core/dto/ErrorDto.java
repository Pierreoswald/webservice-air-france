package com.poswald.core.dto;

import java.util.ArrayList;
import java.util.List;

import com.poswald.core.exception.ErrorCode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Error data acess objet
* 
* @author Poswald
* 
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDto {

	/**
	 *  Http code return (400, 404, 406, 200)
	 */
	private Integer httpCode;
	
	/** 
	 * Type of error code
	 * @see <a href="ErrorCode</a>
	 */
	private ErrorCode errorCode;
	
	/** 
	 * Principal error message 
	 */
	private String message;
	
	/** 
	 * List errors message
	 */
	@Builder.Default
	private List<String> errors = new ArrayList<String>();
	
}
