package com.poswald.core.dto;

import java.time.Instant;

import com.poswald.core.entity.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
* User data acess objet
* 
* @author Poswald
* 
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserDto {

	/**
	 *  User DTO Id
	 */
	private Long id;

	/**
	 *  User login
	 */
	private String login;
	
	/**
	 *  User password
	 */
	private String password;
	
	/**
	 *  User DTO first name
	 */
	private String firstName;

	/**
	 *  User DTO last name 
	 */
	private String lastName;

	/**
	 *  User DTO email 
	 */
	private String email;

	
	/**
	 *  User DTO birth Date
	 */
	private Instant birthDate;

	/**
	 *  User DTO country
	 */
	private String country;
	
	/**
	 *  User DTO default Value
	 *  if enabled, fill in empty fields with default values
	 *  last name: Doe
	 *  first name: John
	 *  birthDate : actual date
	 *  country : France
	 */
	private Boolean activatedDefaultValue;

	/**
	 * Convert User entity in user dto.
	 * 
	 * @param user: User entity
	 * @return User dto
	 */
	public static UserDto fromEntity(User user) {

		if (user == null) {
			return null;
		}

		return UserDto.builder()
				.id(user.getId())
				.login(user.getLogin())
				.password(user.getPassword())
				.email(user.getEmail())
				.lastName(user.getLastName())
				.firstName(user.getFirstName())
				.birthDate(user.getBirthDate())
				.country(user.getCountry())
				.build();
	}

	/**
	 * Convert User dto in user entity.
	 * 
	 * @param userDto: User dto
	 * @return User entity
	 */
	public static User toEntity(UserDto userDto) {

		if (userDto == null) {
			return null;
		}

		return User.builder()
				.id(userDto.getId())
				.login(userDto.getLogin())
				.password(userDto.getPassword())
				.email(userDto.getEmail())
				.lastName(userDto.getLastName())
				.firstName(userDto.getFirstName())
				.birthDate(userDto.getBirthDate())
				.country(userDto.getCountry())
				.build();
	}
}