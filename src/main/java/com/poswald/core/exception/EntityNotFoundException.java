package com.poswald.core.exception;

import lombok.Getter;

/**
* Entity Not Found Exception
* 
* @author Poswald
* 
*/
public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 5539897723983399258L;

	/** 
	 * Type of error code
	 * @see <a href="ErrorCode</a>
	 */
	@Getter
	private ErrorCode errorCode;

	/**
	 * Constructor for entity not found exception
	 * 
	 * @param msg: message error
	 */
	public EntityNotFoundException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for entity not found exception
	 * 
	 * @param msg: message error
	 * @param errorCode: type of error code
	 * @see <a href="ErrorCode</a>
	 */
	public EntityNotFoundException(String msg, ErrorCode errorCode) {
		super(msg);
		this.errorCode = errorCode;
	}

}
