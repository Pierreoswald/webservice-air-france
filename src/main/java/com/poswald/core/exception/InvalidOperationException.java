package com.poswald.core.exception;

import lombok.Getter;

/**
* Invalid Operation Exception
* 
* @author Poswald
* 
*/
public class InvalidOperationException extends RuntimeException {

	private static final long serialVersionUID = -2988918184980400416L;

	/** 
	 * Type of error code
	 * @see <a href="ErrorCode</a>
	 */
	@Getter
	private ErrorCode errorCode;

	/**
	 * Constructor for Invalid Operation Exception
	 * 
	 * @param msg: message error
	 */
	public InvalidOperationException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for Invalid Operation Exception
	 * 
	 * @param msg: message error
	 * @param errorcode: type of error code
	 * @see <a href="ErrorCode</a>
	 */
	public InvalidOperationException(String msg, ErrorCode errorcode) {
		super(msg);
		this.errorCode = errorcode;
	}

}
