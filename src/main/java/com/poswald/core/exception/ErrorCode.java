package com.poswald.core.exception;

public enum ErrorCode {

	USER_NOT_FOUND(1000),
	USER_NOT_VALID(2001),
	USER_ID_NOT_VALID(2002);

	private int code;

	ErrorCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
