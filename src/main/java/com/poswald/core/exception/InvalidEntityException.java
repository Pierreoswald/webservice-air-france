package com.poswald.core.exception;

import java.util.List;

import lombok.Getter;

/**
* Invalid Entity Exception
* 
* @author Poswald
* 
*/
public class InvalidEntityException extends RuntimeException {

	private static final long serialVersionUID = -4985657536931391886L;

	/** 
	 * Type of error code
	 * @see <a href="ErrorCode</a>
	 */
	@Getter
	private ErrorCode errorCode;

	/** 
	 * list of errors messages
	 */
	@Getter
	private List<String> errors;

	public InvalidEntityException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for Invalid Entity Exception
	 * 
	 * @param msg: message error
	 * @param errorcode: type of error code
	 * @see <a href="ErrorCode</a>
	 */
	public InvalidEntityException(String msg, ErrorCode errorcode) {
		super(msg);
		this.errorCode = errorcode;
	}

	/**
	 * Constructor for Invalid Entity Exception
	 * 
	 * @param msg: message error
	 * @param errorcode: type of error code
	 * @see <a href="ErrorCode</a>
	 * @param errors: list of messages
	 */
	public InvalidEntityException(String msg, ErrorCode errorCode, List<String> errors) {
		super(msg);
		this.errorCode = errorCode;
		this.errors = errors;
	}
}
