package com.poswald.core.validator;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.poswald.core.dto.UserDto;
import com.poswald.core.entity.User;

/**
* User Validator
* Validate user data ( not null, size,...)
* 
* @author Poswald
* 
*/
public class UserValidator {

	/**
	 * Validate a User DTO
	 * 
	 * @param user: user to valdate
	 * @return list of errors
	 */
	public static List<String> validate(UserDto user) {
		List<String> errors = new ArrayList<String>();

		if (user == null) {
			errors.add("Please fill user information");
		}

		if (!StringUtils.hasLength(user.getLogin())) {
			errors.add("Please fill login user");
		}

		if (user.getLogin()!=null && user.getLogin().length()>50){
			errors.add("Please enter a login with a maximum of 50 characters");
		}
		
		if (user.getFirstName()!=null && user.getFirstName().length()>50){
			errors.add("Please enter a first name with a maximum of 50 characters");
		}
		
		if (user.getLastName()!=null && user.getLastName().length()>50){
			errors.add("Please enter a last name with a maximum of 50 characters");
		}
		
		if (!StringUtils.hasLength(user.getPassword())) {
			errors.add("Please fill password user");
		}
		
		if (user.getPassword()!=null && user.getPassword().length()>50){
			errors.add("Please enter a password with a maximum of 50 characters");
		}
		
		if (!StringUtils.hasLength(user.getEmail())) {
			errors.add("Please fill email user");
		}
		
		if (user.getEmail()!=null && user.getEmail().length()>70){
			errors.add("Please enter a email with a maximum of 70 characters");
		}
		
		if (user.getBirthDate()==null || user.getBirthDate().isAfter(Instant.now())) {
			errors.add("The date of birth is not valid");
		}

		int age = Period.between(LocalDate.ofInstant(user.getBirthDate(), ZoneOffset.UTC), LocalDate.now()).getYears();

		if (age < 18) {
			errors.add("You must be over 18");
		}

		if (!StringUtils.hasLength(user.getCountry()) || !user.getCountry().equals("France")) {
			errors.add("You must live in france");
		}

		if (user.getCountry()!=null && user.getCountry().length()>50){
			errors.add("Please enter a country with a maximum of 50 characters");
		}
		
		return errors;
	}

}
