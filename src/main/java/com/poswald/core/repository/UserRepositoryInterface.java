package com.poswald.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poswald.core.entity.User;

/**
* User repository
* 
* Manage and access to user data entity
* 
* @author Poswald
* 
*/
public interface UserRepositoryInterface extends JpaRepository<User, Long> {

}
