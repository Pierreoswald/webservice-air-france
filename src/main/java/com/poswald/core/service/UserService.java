package com.poswald.core.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.poswald.core.dto.UserDto;
import com.poswald.core.exception.EntityNotFoundException;
import com.poswald.core.exception.ErrorCode;
import com.poswald.core.exception.InvalidEntityException;
import com.poswald.core.exception.InvalidOperationException;
import com.poswald.core.repository.UserRepositoryInterface;
import com.poswald.core.validator.UserValidator;

import lombok.extern.slf4j.Slf4j;

/**
* User service
* 
* @author Poswald
* access to user management
* 
*/
@Slf4j
@Service
public class UserService {

	private UserRepositoryInterface userRepository;
	
	@Autowired
	public UserService(UserRepositoryInterface userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * Save (Edit or update) User dto
	 * 
	 * @param userDto: User dto to save
	 * @return User Dto update
	 */
	public UserDto save(UserDto user) {
		
		if(user!=null && user.getActivatedDefaultValue()!=null && user.getActivatedDefaultValue()) {
			log.error("Default value is used");
			user = DefaultValueUser(user);
		}
		
		List<String> errors = UserValidator.validate(user);

		if (!errors.isEmpty()) {
			log.error("User is not valid {}", user);
			errors.stream().forEach(e -> log.error("error: "+e));
			throw new InvalidEntityException("User is not valid", ErrorCode.USER_NOT_VALID, errors);
		}

		return UserDto.fromEntity(userRepository.save(UserDto.toEntity(user)));
	}

	/**
	 * Find by id a User dto
	 * 
	 * @param id: identifiant of user to find
	 * @return User Dto with the id
	 */
	public UserDto findById(Long id) {

		if (id == null || id.equals(0L) || id < 0L) {
			log.error("User id is not valid");
			throw new InvalidOperationException("User id is not valid", ErrorCode.USER_ID_NOT_VALID);
		}

		return userRepository.findById(id)
				.map(UserDto::fromEntity)
				.orElseThrow(() -> new EntityNotFoundException("No user with the id: " + id, ErrorCode.USER_NOT_FOUND));
	}

	/**
	 * Fill with user default value
	 * 
	 * @param user
	 * @return User Dto with default value
	 */
	private UserDto DefaultValueUser(UserDto user) {

		if (!StringUtils.hasLength(user.getLastName())) {
			user.setLastName("Doe");
		}

		if (!StringUtils.hasLength(user.getFirstName())) {
			user.setFirstName("Jhon");
		}
		
		if (user.getBirthDate()==null) {
			user.setBirthDate(LocalDateTime.now().minusYears(19).toInstant(ZoneOffset.UTC));
		}

		if (!StringUtils.hasLength(user.getCountry())) {
			user.setCountry("France");
		}
		
		if (!StringUtils.hasLength(user.getLogin())) {
			user.setLogin("Login");
		}
		
		if (!StringUtils.hasLength(user.getEmail())) {
			user.setEmail("email");
		}
		
		if (!StringUtils.hasLength(user.getPassword())) {
			user.setPassword("1234");
		}
		
		return user;
	}
}
